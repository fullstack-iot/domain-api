# ---- Base Node ----
FROM node:20.11.0 AS base
WORKDIR /app
COPY package.json pnpm-lock.yaml cfg.yml ./
RUN npm install -g npm@10.3.0
RUN npm install -g pnpm

# ---- Dependencies ----
FROM base AS dependencies
RUN pnpm install --frozen-lockfile

# ---- Build ----
FROM dependencies AS build
COPY . .
RUN pnpm run build

# ---- Release ----
FROM node:20.11.0 AS release
WORKDIR /app
COPY --from=build /app/dist ./dist
COPY --from=build /app/cfg.yml ./
COPY --from=dependencies /app/node_modules ./node_modules
COPY package.json ./
CMD [ "node", "dist/main" ]
