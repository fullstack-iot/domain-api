import { deepStrictEqual as assertEqual } from "assert";
import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import request, { Response } from "supertest";
import AppModule from "../src/app.module";
import {
  PostgreSqlContainer,
  StartedPostgreSqlContainer,
} from "@testcontainers/postgresql";

// eslint-disable-next-line max-lines-per-function
describe("AppController (integration)", () => {
  let app: INestApplication;
  let container: StartedPostgreSqlContainer;

  beforeAll(async () => {
    // Start PostgreSQL container
    container = await new PostgreSqlContainer()
      .withDatabase("core")
      .withUsername("postgres")
      .withPassword("placeholder")
      .withExposedPorts({ container: 5432, host: 5432 })
      .start();

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider("configService")
      .useValue({
        get: jest.fn((key: string) => {
          if (key === "sqlHost") return container.getHost();
          return process.env[key];
        }),
      })
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  }, 60000); // Increase timeout for container startup

  afterAll(async () => {
    await app.close();
    await container.stop();
  });

  test("get status", async () => {
    const actual = (await request(app.getHttpServer()).get("/")) as Response;
    const expected = "Running 🚀";

    assertEqual(actual.text, expected);
  });
});
