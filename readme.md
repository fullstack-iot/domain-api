# Domain API 🌊

![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=02-boolean-reading&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/badge/node-20.11.0-gray?style=for-the-badge&logo=node.js)
![](https://img.shields.io/badge/typescript-5.5.3-gray?style=for-the-badge&logo=typescript)
![](https://img.shields.io/badge/web_framework-nestjs-E0234E?style=for-the-badge)
![](https://img.shields.io/badge/ORM-typeorm-5DBE5C?style=for-the-badge)

## 📖 About

The API is designed to manage a structure of devices. The devices could be hierarchical if necessary. The structure is organized as follows:

- **Devices**: There can be many devices. They can be nested. Currently, this api is only two levels deep, for example, `device-0` and `device-1`, but the structure could go deeper in the future.

### API Endpoints

The API provides several endpoints to manage this structure:

- **Devices-0**: Devices at level 0 can be accessed at the `/devices-0/:device0Id` endpoint.
- **Devices-1**: Devices at level 1 can be accessed at the `/devices-0/:device0Id/devices-1/device1Id` endpoint. In this scenario `device0Id` is the parent device and `device1Id` is the child device.

### Future Enhancements

While the current structure supports up to two levels of devices, future enhancements may include support for deeper device levels.

Please refer to the individual endpoint documentation for more details on how to use these endpoints.
