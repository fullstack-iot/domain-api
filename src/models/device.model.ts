import { IsString, IsArray, ValidateNested, Matches } from "class-validator";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

const TOPIC_REGEX = /^[^+#/]+(?:\/[^+#/]+)*$/;
const DEVICE_ID_REGEX = /^[a-z_]+-\d+$/i;

// Single point data aka data about a single point. e.g. topic and name.
// Pluralize it to point data eg. pointData.map((pointDatum) => pointDatum.name)
export class PointDatum {
  @ApiProperty({
    pattern: String(TOPIC_REGEX),
    example: "modbus-1/status",
  })
  @Matches(TOPIC_REGEX, {
    message: "topic must be a valid MQTT topic",
  })
  @IsString()
  topic!: string;

  @ApiProperty({
    type: String,
    example: "status",
  })
  @IsString()
  name!: string;
}

export class Device {
  @ApiProperty({ type: [PointDatum] })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PointDatum)
  pointData!: PointDatum[];

  @ApiProperty({
    pattern: String(DEVICE_ID_REGEX),
    example: "modbus-1",
  })
  @IsString()
  @Matches(DEVICE_ID_REGEX, {
    message:
      "id must be a valid format: alphabetic characters or underscores, followed by a hyphen then digits",
  })
  id!: string;

  @ApiProperty({
    type: [String],
    pattern: String(DEVICE_ID_REGEX),
    example: ["modbus-1", "modbus-2"],
  })
  @IsArray()
  @Matches(DEVICE_ID_REGEX, {
    each: true,
    message:
      "Each id must be a valid format: alphabetic characters or underscores, followed by a hyphen then digits",
  })
  subDeviceIds!: string[];
}
