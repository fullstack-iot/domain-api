import { Controller, Get } from "@nestjs/common";
import AppService from "./app.service.js";

@Controller()
class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  status(): string {
    return this.appService.status();
  }
}

export default AppController;
