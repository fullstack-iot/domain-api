import { Injectable } from "@nestjs/common";

@Injectable()
class AppService {
  status(): string {
    return "Running 🚀";
  }
}

export default AppService;
