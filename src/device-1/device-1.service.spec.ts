import { Test, TestingModule } from "@nestjs/testing";
import { Device1Service } from "./device-1.service";

describe("Device1Service", () => {
  let service: Device1Service;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Device1Service],
    }).compile();

    service = module.get<Device1Service>(Device1Service);
  });

  test("should be defined", () => {
    expect(service).toBeDefined();
  });
});
