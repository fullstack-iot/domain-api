import { Controller, Get, Param } from "@nestjs/common";
import { Device1Service } from "./device-1.service.js";

@Controller("devices-0/:device0Id/devices-1")
export class Device1Controller {
  constructor(private readonly device1Service: Device1Service) {}

  @Get(":device1Id")
  findOne(
    @Param("device0Id") device0Id: string,
    @Param("device1Id") device1Id: string,
  ): string {
    return this.device1Service.findOne(device0Id, device1Id);
  }
}
