import { Injectable } from "@nestjs/common";

@Injectable()
export class Device1Service {
  findOne(device0Id: string, device1Id: string): string {
    return `This action returns a ${device1Id} device for the ${device0Id}  parent`;
  }
}
