import { Module } from "@nestjs/common";
import { Device1Controller } from "./device-1.controller.js";
import { Device1Service } from "./device-1.service.js";

@Module({
  controllers: [Device1Controller],
  providers: [Device1Service],
})
export class Device1Module {}
