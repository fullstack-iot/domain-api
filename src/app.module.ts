import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Device0Module } from "./device-0/device-0.module.js";
import { Device1Module } from "./device-1/device-1.module.js";
import AppController from "./app.controller.js";
import AppService from "./app.service.js";
import loadConfig from "./utils.js";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [loadConfig],
    }),
    Device0Module,
    Device1Module,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: "postgres",
        host: configService.get("sqlHost", "0.0.0.0"),
        port: 5432,
        username: "postgres",
        password: process.env["SQL_PASSWORD"] || "placeholder",
        database: "postgres",
        autoLoadEntities: true,
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
class AppModule {}

export default AppModule;
