import { promises as fs } from "fs";
import { z } from "zod";
import { Address4 as IpV4 } from "ip-address";
import { match } from "ts-pattern";
import yaml from "yaml";

const Config = z.object({
  logLevel: z.union([
    z.literal("ERROR"),
    z.literal("WARN"),
    z.literal("INFO"),
    z.literal("DEBUG"),
  ]),
  port: z.number().min(80).max(65535),
  host: z.string().transform((val) => new IpV4(val).address),
  sqlHost: z.string().transform((val) => new IpV4(val).address),
});

type ConfigType = z.infer<typeof Config>;

interface ConfigMap {
  development: ConfigType;
  staging: ConfigType;
  production: ConfigType;
}

async function loadConfig(): Promise<ConfigType> {
  const file = await fs.readFile("cfg.yml", "utf8");
  const configMap = yaml.parse(file) as ConfigMap;
  const environment = process.env["ENV"] || "development";
  return match(environment)
    .with("production", () => Config.parse(configMap.production))
    .with("staging", () => Config.parse(configMap.staging))
    .otherwise(() => Config.parse(configMap.development));
}

export default loadConfig;
