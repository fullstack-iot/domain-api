import { Test, TestingModule } from "@nestjs/testing";
import { Device0Service } from "./device-0.service";

describe("Device0Service", () => {
  let service: Device0Service;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Device0Service],
    }).compile();

    service = module.get<Device0Service>(Device0Service);
  });

  test("should be defined", () => {
    expect(service).toBeDefined();
  });
});
