import { Injectable } from "@nestjs/common";
import { Device } from "../models/device.model.js";

const deviceDb = [
  {
    id: "modbus-1",
    pointData: [
      {
        name: "status",
        topic: "modbus-1/status",
      },
      {
        name: "voltage",
        topic: "modbus-1/voltage",
      },
    ],
  },
] as Device[];

@Injectable()
export class Device0Service {
  findOne(device0Id: string): Device {
    return deviceDb.find((device) => device.id === device0Id)!;
  }
}
