import { Module } from "@nestjs/common";
import { Device0Controller } from "./device-0.controller.js";
import { Device0Service } from "./device-0.service.js";

@Module({
  controllers: [Device0Controller],
  providers: [Device0Service],
})
export class Device0Module {}
