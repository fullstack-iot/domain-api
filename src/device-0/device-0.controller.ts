import { Controller, Get, Param } from "@nestjs/common";
import { ApiOkResponse } from "@nestjs/swagger";
import { Device0Service } from "./device-0.service.js";
import { Device } from "../models/device.model.js";

@Controller("devices-0")
export class Device0Controller {
  constructor(private readonly device0Service: Device0Service) {}

  @Get(":device0Id")
  @ApiOkResponse({ type: Device })
  findOne(@Param("device0Id") device0Id: string): Device {
    return this.device0Service.findOne(device0Id);
  }
}
